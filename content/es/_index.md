---
title: "Abierta URJC"

description: "Materiales en abierto en la URJC"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

{{< columns >}} <!-- begin columns block -->
## Monografías
[Colección de monografías publicadas en abierto](https://omp.urjc.es/). 

<---> <!-- magic separator, between columns -->

## Revistas
[Colección de revistas publicadas en abierto](https://ojs.urjc.es/)

<---> <!-- magic separator, between columns -->

## Asignaturas
[Asignaturas en abierto](https://aulavirtual.urjc.es/)

<--->

## Materiales docentes
[Colección de materiales docentes publicados en abierto](https://burjcdigital.urjc.es/handle/10115/19268)
{{< /columns >}}